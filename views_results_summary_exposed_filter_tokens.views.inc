<?php

/**
 * @file
 * A file with Views hook implementations.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_results_summary_exposed_filter_tokens_views_data_alter(&$data) {
  $data['views']['result_tokens'] = array(
    'title' => t('Result summary with tokens'),
    'help' => t('Shows result summary, for example the items per page.'),
    'area' => array(
      'handler' => 'views_results_summary_exposed_filter_tokens_handler_area_result',
    ),
  );
}