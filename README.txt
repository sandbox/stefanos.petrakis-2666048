DESCRIPTION
===========
Provides an extended version of the result summary area, that allows the use of exposed filters as tokens.

SITE BUILDERS
=============
You can add this result summary handler to your view's header, 
footer or "no results behavior" regions.
The name of the handler is "Global: Result summary with tokens".
You will see the list of tokens that are related to
the exposed filters in your view.
They are prefixed with "eft_", e.g. @eft_status
